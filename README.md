# README #

DMR-Seeker is a software which take CGMap output from BS-Seeker2 and find the Differentially Methylated Regions for you.


### What is this repository for? ###

* DMR-Seeker is a software which take CGMap output from BS-Seeker2 and find the Differentially Methylated Regions for you.
* The idea is original from Pao-Yang Chen, implemented and improved by Yih-Shien Chiang.
* Version: 0.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Dependencies

  JAVA

* Summary of set up

  This software is written in Java, you'll need JRE or JDK to run it. 
  (Tested with java version "1.8.0_65" under Scientific Linux and "1.8.0_66" under Mac OS X Yosemite)

* How to run tests

  test files included in this package: mt1.CGmap.CG, mt2.CGmap.CG, mt3.CGmap.CG, wt1.CGmap.CG, wt2.CGmap.CG, wt3.CGmap.CG
command to run DMR-Seeker with test files:
  > java -jar DMR-Seeker.jar [-bed] -peak 0.2 -ext 0.0 -zscore 10 -width 10 -FDR 0.1 -E mt1.CGmap.CG mt2.CGmap.CG mt3.CGmap.CG -C wt1.CGmap.CG wt2.CGmap.CG wt3.CGmap.CG

* How to convert DMR-Seeker output to bed format

  command below will output not only final result but also detail information
  > java -jar DMR-Seeker.jar -peak 0.2 -ext 0.0 -zscore 10 -width 10 -FDR 0.1 -E mt1.CGmap.CG mt2.CGmap.CG mt3.CGmap.CG -C wt1.CGmap.CG wt2.CGmap.CG wt3.CGmap.CG

  command below will output final result in bed format without any other information
  > java -jar DMR-Seeker.jar -bed -peak 0.2 -ext 0.0 -zscore 10 -width 10 -FDR 0.1 -E mt1.CGmap.CG mt2.CGmap.CG mt3.CGmap.CG -C wt1.CGmap.CG wt2.CGmap.CG wt3.CGmap.CG

  In the future, DMR-Seeker will support different output formats.

* Deployment instructions

  Copy jar file to your destination and run it with JRE.

### Parameters ###

* bed: output final result in bed format without any other information. optional.
* peak: methylation level larger then this value will be considered peak. between 0.0 and 1.0
* ext: methylation level larger then this value will be considered extensible. between 0.0 and 1.0
* zscore: zscore smaller then this value will not be reported.
* width: DMR with width shorter then this number will be filtered out.
* FDR: false discovery rate. zscore filter will be calculated based on this value. between 0.0 and 1.0
* E: experiment group, file names separated by space. (minimum requirement, two files, in CGmap format)
* C: control group, file names separated by space. (minimum requirement, two files, in CGmap format)

### Output format ###
#### default ####
output simulated data, output real data, output sorted zscores, output FDR information, output final result.
#### bed ####
* chr
* start
* end
* length
* zscore

### Simulated data ###

Following files are under example folder
* mt1.CGmap.CG, mt2.CGmap.CG, mt3.CGmap.CG: CGmap files for experiment samples
* wt1.CGmap.CG, wt2.CGmap.CG, wt3.CGmap.CG: CGmap files for control samples
* DMR_region.txt: contains information about manipulated regions.

### FAQ ###

* How to calculate DMR for CG, CHG, CHH separately?

  Separate your CGmap to CG, CHG, and CHH first then run DMR-Seeker separately.
  For example, to get CG from mt1.CGmap and save to mt1.CGmap.CG in linux:
  > cat mt1.CGmap | grep CG > mt1.CGmap.CG

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Yih-Shien Chiang, markchiang at gmail.com
* Pao-Yang Chen, paoyang@gate.sinica.edu.tw