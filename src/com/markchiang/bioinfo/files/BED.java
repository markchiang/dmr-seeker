/**
 * 
 */
package com.markchiang.bioinfo.files;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import htsjdk.tribble.CloseableTribbleIterator;
import htsjdk.tribble.bed.BEDCodec;
import htsjdk.tribble.bed.BEDCodec.StartOffset;
import htsjdk.tribble.bed.BEDFeature;
import htsjdk.tribble.readers.AsciiLineReader;
import htsjdk.tribble.readers.LineIteratorImpl;
/**
 * @author markchiang -Dosgi.requiredJavaVersion=1.7
 *
 */
public class BED {
	List<BEDFeature> allFeatures;
    int x;
    HashMap<String, List<BEDFeature>> chrToBF;
    
	/**
	 * 
	 */
	public BED(File bed) throws IOException {
		// TODO Auto-generated constructor stub
	    chrToBF = new HashMap<String, List<BEDFeature>>();
        allFeatures = new ArrayList<BEDFeature>();
        InputStream is = new java.io.FileInputStream(bed);
        AsciiLineReader lr = new AsciiLineReader(is);
        LineIteratorImpl li = new LineIteratorImpl(lr);
        BEDCodec bc = new BEDCodec(StartOffset.ZERO);
        while(!bc.isDone(li))
        {
            BEDFeature bf = bc.decode(li);
            if(bf != null)
            {
                allFeatures.add(bf);
                if(chrToBF.containsKey(bf.getChr()))
                {
                    chrToBF.get(bf.getChr()).add(bf);
                }else
                {
                    List<BEDFeature> a = new ArrayList<BEDFeature>();
                    a.add(bf);
                    chrToBF.put(bf.getChr(), a);
                }
            }
        }
        this.x = allFeatures.size();
	}

	public List<BEDFeature> allFeatures(){
		return allFeatures;
	}

	public List<BEDFeature> allFeatures(String chr){
		return chrToBF.get(chr);
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BED bed = new BED(new File("example/hg19_CpGIslands.bed"));
		
		List<BEDFeature> iter=bed.allFeatures("chr1");
		for (BEDFeature bedFeature : iter) {
			System.out.println(bedFeature.getContig()+":"+bedFeature.getStart()+"-"+bedFeature.getEnd());
		}
	}

}