/**
 * refGene.txt reader
 */
package com.markchiang.bioinfo.files;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import com.markchiang.bioinfo.methylation.GenomeRegion;

/**
 * @author markchiang
 *
 */
public class RefGene {

	//TODO: improve performance using TreeMap. 
	List<Gene> genes = new ArrayList<Gene>();
	
	/**
	 * @param filename
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public RefGene(String filename) throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(filename))));
		for (String s; (s = br.readLine()) != null;) {
            String[] fields = s.split("\t");

            String id = fields[1];
            String chr = fields[2];
            String strand = fields[3];
            int start = Integer.parseInt(fields[4]);
            int end = Integer.parseInt(fields[5]);
            String symbol = fields[12];
            genes.add(new Gene(id,chr,strand,start,end,symbol));
        }
		br.close();
	}

	public class Gene {
		String id = null;
		String chr = null;
		String strand = null;
		Integer start = null;
		Integer end = null;
		String symbol = null;
		public Gene(String id, String chr, String strand, Integer start, Integer end, String symbol) {
			this.id = id;
			this.chr = chr;
			this.strand = strand;
			this.start = start;
			this.end = end;
			this.symbol = symbol;
		}
	}
	
	public List<Gene> overlaped(GenomeRegion region){
		List<Gene> answer = new ArrayList<Gene>();
		for (Gene gene : genes) {
			if (!gene.chr.equals(region.getChr())){
				continue;
			}else if ((gene.start<=region.getEnd())&&(gene.start>=region.getStart())){ // region overlapped gene's left part
				answer.add(gene);
			}else if ((gene.start<=region.getStart())&&(gene.end>=region.getEnd())){ // region overlapped by genes
				answer.add(gene);				
			}else if ((gene.end>=region.getStart())&&(gene.end<=region.getEnd())){ // region overlapped gene's right part
				answer.add(gene);
			}
		}
		return answer;
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		RefGene annotation = new RefGene("example/refGene_hg19.txt.gz");
		System.out.println(annotation.overlaped(new GenomeRegion("chr1",1,1000000000)).size());
		System.out.println(annotation.overlaped(new GenomeRegion("chr2",1,1000000000)).size());
		System.out.println(annotation.overlaped(new GenomeRegion("1",1,1000000000)).size());
		System.out.println(annotation.overlaped(new GenomeRegion("chr1",1,1000000000)).get(1).id);
		System.out.println(annotation.overlaped(new GenomeRegion("chr1",1,1000000000)).get(1).symbol);
	}
}
