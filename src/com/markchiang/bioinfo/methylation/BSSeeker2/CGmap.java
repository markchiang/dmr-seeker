/**
 * 
 */
package com.markchiang.bioinfo.methylation.BSSeeker2;

import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author markchiang
 *
 */
public class CGmap {

	String filename;
	
	TreeMap<String,TreeMap<Integer,MethylationInfo>> chrMap= new TreeMap<String,TreeMap<Integer,MethylationInfo>>();
	//following treemap will be generated in the while loop for each chr
	//TreeMap<Integer,Number> treeMap= new TreeMap<Integer, Number>();	
	/**
	 * 
	 */
	public CGmap() {
		// TODO Auto-generated constructor stub
	}

	public TreeMap<String,TreeMap<Integer,MethylationInfo>> getChrMap(){
		return chrMap;
	}
	
	public CGmap(String filename) throws IOException{
		this.filename = filename;
		// construct a CGmap object from BSSeeker2 CGmap file
		BufferedReader br = new BufferedReader((new FileReader(filename)));	
		String line;
		while ((line=br.readLine())!=null){
			String fields[] = line.split("\t");
			String chr = fields[0];
			Integer pos = Integer.parseInt(fields[2]);
			double methylationLevel = Double.parseDouble(fields[5]);
			int c = Integer.parseInt(fields[6]);
			int coverage = Integer.parseInt(fields[7]);
			MethylationInfo info = new MethylationInfo(c, methylationLevel, coverage);
			
			if (!chrMap.containsKey(chr)){
			  chrMap.put(chr, new TreeMap<Integer,MethylationInfo>());	
			}
			TreeMap<Integer,MethylationInfo> treeMap = chrMap.get(chr);
			treeMap.put(pos,info);
		}
		br.close();
	}

	/**
	 * return the mean value of query region
	 * @param chr
	 * @param start
	 * @param end
	 * @return
	 */
	public double mean(String chr, Integer start, Integer end){
		if (chrMap.get(chr)==null) 
			return Double.NaN;
		SortedMap<Integer,MethylationInfo> subset = chrMap.get(chr).subMap(start, end);
		double sum=0.0;
		int count=0;
		for (MethylationInfo info : subset.values()) {
			sum+=info.getMethylationLevel(false);
			count++;
		}
		return sum/count;
	}

	public Set<String> getChromosomes(){
		return chrMap.keySet();
	}
	
	public Integer maxPosition(String chr){
		return chrMap.get(chr).lastKey();
	}
	
	public MethylationInfo getMethylationInfo(String chr, Integer pos){
		if (chrMap.get(chr) == null) return null;
		else return chrMap.get(chr).get(pos);
	}

	@Override
	public String toString(){
		return filename;
	}
}