/**
 * 
 */
package com.markchiang.bioinfo.methylation.BSSeeker2;

/**
 * @author markchiang -Dosgi.requiredJavaVersion=1.7
 *
 */
public class MethylationInfo {
	int numberOfC;
	int simulatedNumberOfC;
	double methylationLevel;
	double simulatedLevel;
	int coverage;
	
	/**
	 * @return the simulatedNumberOfC
	 */
	public int getSimulatedNumberOfC() {
		return simulatedNumberOfC;
	}

	/**
	 * @param simulatedNumberOfC the simulatedNumberOfC to set
	 */
	public void setSimulatedNumberOfC(int simulatedNumberOfC) {
		this.simulatedNumberOfC = simulatedNumberOfC;
		setSimulatedLevel((double)simulatedNumberOfC/coverage);
	}

	/**
	 * @return the simulatedLevel
	 */
	public double getSimulatedLevel() {
		return simulatedLevel;
	}

	/**
	 * @param simulatedLevel the simulatedLevel to set
	 */
	private void setSimulatedLevel(double simulatedLevel) {
		this.simulatedLevel = simulatedLevel;
	}

	/**
	 * @param numberOfC
	 * @param methylationLevel
	 * @param coverage
	 */
	public MethylationInfo(int c, double methylationLevel, int coverage) {
		super();
		this.numberOfC = c;
		this.methylationLevel = methylationLevel;
		this.coverage = coverage;
	}

	/**
	 * @return the numberOfC
	 */
	public int getNumberOfC() {
		return numberOfC;
	}

	/**
	 * @return the numberOfC
	 */
	public int getNumberOfT() {
		return coverage-numberOfC;
	}

	/**
	 * @return the methylationLevel
	 */
	public double getMethylationLevel(boolean simulation) {
		if (simulation) 
			return getSimulatedLevel();
		else
			return methylationLevel;
	}

	/**
	 * @return the coverage
	 */
	public int getCoverage() {
		return coverage;
	}
	
	
}
