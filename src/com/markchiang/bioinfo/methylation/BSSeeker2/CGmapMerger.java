/**
 * This class will take multiple CGmap file and merge data into one big table.
 * Table	Lib1	Lib2
 * Loci1	Val11	Val21
 * Loci2	Val12	Val22
 * ...
 */
package com.markchiang.bioinfo.methylation.BSSeeker2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author markchiang
 *
 */
public class CGmapMerger {
	
	TreeMap<String,TreeSet<Integer>> UnionLocis = new TreeMap<String,TreeSet<Integer>>();
	List<CGmap> allCGmaps = new ArrayList<CGmap>();
	
	public void merge(CGmap cgmap){
		TreeMap<String,TreeMap<Integer,MethylationInfo>> treeMap = cgmap.getChrMap();
		Set<String> chrs = cgmap.getChromosomes();
		for (String chr : chrs) {
			if (!UnionLocis.containsKey(chr)) 
				UnionLocis.put(chr, new TreeSet<Integer>());
			Set<Integer> unionLocisOnThisChr = UnionLocis.get(chr);			
			TreeMap<Integer,MethylationInfo> lociValues = treeMap.get(chr);
			Set<Integer> locisInThisSample = lociValues.keySet();
			unionLocisOnThisChr.addAll(locisInThisSample);
		}//now we got all locis from multiple CGmaps
		allCGmaps.add(cgmap);//and CGmaps data stored.
	}

	/**
	 * if head = 0, print all results.
	 * if head > 0, print #head lines of results.
	 * if outputLinesWithNullInIt = true, print all result.
	 * if outputLinesWithNullInIt = false, skip records with null in it.
	 * @param head
	 * @param outputLinesWithNullInIt
	 */
	public void result(int head, boolean outputLinesWithNullInIt){
		int lines = 0;
		Set<String> chrs = UnionLocis.keySet();
		for (String chr : chrs) {
			for (Integer loci : UnionLocis.get(chr)) {
				lines++;
				boolean hasNull=false;
				StringJoiner joiner = new StringJoiner("\t");
				joiner.add(chr);
				joiner.add(loci.toString());
				for (CGmap cgmap : allCGmaps){
					
					if (cgmap.getChrMap().get(chr).get(loci)!=null){
						Number value = cgmap.getChrMap().get(chr).get(loci).getMethylationLevel(false);
						joiner.add(value.toString());
					}else{
						hasNull=true;
						joiner.add("");
					}
				}
				if ((!outputLinesWithNullInIt)&&(hasNull)) continue;
				System.out.println(joiner.toString());
				if ((head>0) && (lines >= head)) return; 
			}
		}
	}
	
	
	/**
	 * @return the unionLocis
	 */
	public TreeMap<String, TreeSet<Integer>> getUnionLocis() {
		return UnionLocis;
	}

	/**
	 * @return the allCGmaps
	 */
	public List<CGmap> getAllCGmaps() {
		return allCGmaps;
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// Example as unit test
		CGmap cgmap1 = new CGmap("example/69.head.CGmap");
		CGmap cgmap2 = new CGmap("example/70.head.CGmap");
		CGmap cgmap3 = new CGmap("example/71.head.CGmap");
		CGmapMerger merger = new CGmapMerger();
		merger.merge(cgmap1);
		merger.merge(cgmap2);
		merger.merge(cgmap3);
		merger.result(0,false);
	}

}
