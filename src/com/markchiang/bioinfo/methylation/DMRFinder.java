/**
 * 
 */
package com.markchiang.bioinfo.methylation;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;

import org.apache.commons.math3.stat.inference.TTest;

import com.markchiang.bioinfo.methylation.BSSeeker2.CGmap;

/**
 * DMRFinder looks for differential methylated regions for you.
 * @author markchiang
 * 
 */
public class DMRFinder {

	/**
	 * 
	 * @param cgmap1
	 * @param cgmap2
	 * @param windowlength
	 * @param thresholdDelta	Only data with Delta larger then this number will be reported
	 * @param thresholdPValue	Only data with p-value less then this number will be reported
	 */
	public DMRFinder(CGmap cgmap1, CGmap cgmap2, int windowlength, double thresholdDelta, double thresholdPValue) {
		// TODO: change algorithm from using bin width to using window length

		// we need to know how many chromosomes we need to loop through
		Set<String> chrs = new HashSet<String>();
		chrs.addAll(cgmap1.getChromosomes());
		chrs.addAll(cgmap2.getChromosomes());
		//now chrs contains all Chromosomes (union) of these two subsets(cm1,cm2).

		for (String chr : chrs) {
			// calculate 1000 potential DMRs with bin width 100 and report DMRs if delta >= threshold
			// TODO: change algorithm from using bin width to using window length
			// TODO: we need to calculate p-value for DMR, probably I should implement a DMRFinder.p-value(CGmap1, CGmap2, GenomeRegion)
			// we need to know the max position of each chromosome in order to determin how many loops we need to run.
			int max = cgmap1.maxPosition(chr);
			int loop = max / windowlength;
			for (int cx=0; cx<=loop; cx++){
				int start=windowlength*cx;
				int end=windowlength*(cx+1)-1;
				double value1=cgmap1.mean(chr,start,end);
				double value2=cgmap2.mean(chr,start,end);
				double difference=value1-value2;
				double delta=Math.abs(difference);
				if (delta>=thresholdDelta){
					double p_value=p_value(cgmap1,cgmap2,chr,start,end);
					System.out.println(chr+"\t"+start+"\t"+end+"\t"+value1+"\t"+value2+"\t"+difference+"\t"+p_value);
				}
			}			
		}
	};

	public double p_value(CGmap cgmap1, CGmap cgmap2, String chr, Integer start, Integer end){
		Object[] objects1 = cgmap1.getChrMap().get(chr).subMap(start, end).values().toArray();
		Object[] objects2 = cgmap2.getChrMap().get(chr).subMap(start, end).values().toArray();
		//p-value needs at least two numbers to calculate t statistic.
		if ((objects1.length<2)||(objects2.length<2)) return 1;
		double[] values1 = new double[objects1.length];
		double[] values2 = new double[objects2.length];
		for (int cx=0; cx<objects1.length; cx++) {
			values1[cx]=(Double)objects1[cx];
		}
		for (int cx=0; cx<objects2.length; cx++) {
			values2[cx]=(Double)objects2[cx];
		}
		return new TTest().tTest(values1,values2);	//p-value == NaN means p-value=0.0?
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// This example finds DMR for 70.head.CGmap vs 71.head.CGmap
		// read 70.head.CGmap
		CGmap cm1 = new CGmap("example/70.head.CGmap");
		// read 71.head.CGmap
		CGmap cm2 = new CGmap("example/71.head.CGmap");

		DMRFinder dmrfinder = new DMRFinder(cm1, cm2, 50, 0.2, 0.05);
		
	}

}