/**
 * 
 */
package com.markchiang.bioinfo.methylation;

import com.markchiang.bioinfo.methylation.DMRSeeker.GenomeLocation;

/**
 * @author markchiang
 *
 */
public class GenomeRegion implements Comparable{
	String chr;
	Integer start;
	/**
	 * @return the chr
	 */
	public String getChr() {
		return chr;
	}

	/**
	 * @return the start
	 */
	public Integer getStart() {
		return start;
	}

	/**
	 * @return the end
	 */
	public Integer getEnd() {
		return end;
	}

	Integer end;
	/**
	 * @param chr	Chromosome of this region
	 * @param start	Chromosome start of this region
	 * @param end	Chromosome end of this region
	 */
	public GenomeRegion(String chr, Integer start, Integer end) {
		super();
		this.chr = chr;
		this.start = start;
		this.end = end;
	}
	
	@Override
	public int compareTo(Object other) {
		GenomeRegion otherRegion = (GenomeRegion)other;
		if (chr.equals(otherRegion.chr)){
			if (start.compareTo(otherRegion.start)==0)
				return end.compareTo(otherRegion.end);
			else 
				return start.compareTo(otherRegion.start);
		}else
			return chr.compareTo(otherRegion.chr);
	}
	
	@Override
	public String toString(){
		return (chr+":"+start+"-"+end);
	}

	public int length() {
		return end-start;
	}

	public boolean contains(GenomeLocation location){
		return location.inThisRegion(this);
	}
	public String toBed(){
		return (chr+"\t"+start+"\t"+end);
	}
}