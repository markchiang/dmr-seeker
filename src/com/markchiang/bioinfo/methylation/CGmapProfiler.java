/**
 * 
 */
package com.markchiang.bioinfo.methylation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.broad.igv.bbfile.BBFileReader;

import com.markchiang.bioinfo.files.BED;
import com.markchiang.bioinfo.methylation.BSSeeker2.CGmap;

import htsjdk.tribble.*;
import htsjdk.tribble.bed.BEDCodec;
import htsjdk.tribble.bed.BEDFeature;

/**
 * CGmapProfiler contains gene profiler, exon profiler, intron profiler, CpG islands profiler, etc. It take CGmap file from BS Seeker as input and calculate profiles using different profilers.
 * @author markchiang
 *
 */
public class CGmapProfiler {

	CGmap cgmap;

	/**
	 * Create a CGmapProfiler using a CGmap object,
	 * so you can query the profile later using different profiler provided in this Class.
	 */
	public CGmapProfiler(CGmap cgmap) {
		// TODO Auto-generated constructor stub
		this.cgmap = cgmap;
	}

	/**
	 * return the profile for query region (if the mean is NaN, it will skip that region)
	 * @param windowlen
	 * @param chr
	 * @param start
	 * @param end
	 */
	public void windowProfiler(int windowlen, String chr, int start, int end){
		for (int cx=0; cx<=((end-start)/windowlen); cx++){
			int windowstart=start+windowlen*cx;
			int windowend=start+windowlen*(cx+1)-1;
			//TODO: change from system.out to return results.
			double mean = cgmap.mean(chr,windowstart,windowend);
			if (!Double.isNaN(mean)){
				System.out.println(chr+"\t"+start+"\t"+end+"\t"+windowstart+"\t"+windowend+"\t"+mean);
			}
		}		
	}

	/**
	 * return profiles for genome regions, you can use this as gene profiler, CpG islands profiler, by providing gene regions, or CpG regions.
	 * Since this profiler will have different window length, the windowlen is calculate dynamically. 
	 * @param regions
	 */
	public void genomeRegionsProfiler(int number_of_windows, List<GenomeRegion> regions){
		//TODO: genomeRegion should have directions, so we can draw by gene direction.
		for (GenomeRegion genomeRegion : regions) {
			int start = genomeRegion.start;
			int end = genomeRegion.end;
			int windowlen = (end-start+1)/number_of_windows;
			windowProfiler(windowlen, genomeRegion.chr, start, end); //output tabular format per line for each region
		}
	}
	
	/**
	 * return the profiles of all Chromosomes
	 * @param windowlen
	 */
	public void windowProfiler(int windowlen){
		for (String chr : cgmap.getChromosomes()) {
			int start = 1;
			int end = cgmap.getChrMap().get(chr).lastKey();
			windowProfiler(windowlen,chr,start,end);
		}
	}

	public void bedFeaturesProfiler(int number_of_windows, String bedfilename) throws IOException{
		List<GenomeRegion> regions = new ArrayList<GenomeRegion>();
		BED bed = new BED(new File(bedfilename));
		List<BEDFeature> iter=bed.allFeatures();
		for (BEDFeature bedFeature : iter) {
			String chrStr=bedFeature.getContig();
			String chr="";
			//TODO: following 4 lines are for demonstration, the conversion should be done outside of this program.
			boolean bedStartWithChr = chrStr.startsWith("chr");
			boolean CGmapStartWithChr = ((String)(cgmap.getChromosomes().toArray()[0])).startsWith("chr");
			if (bedStartWithChr&&!CGmapStartWithChr){	//in hg19_CpGIslands.bed, the format is chr1, chr2, etc.
				chr=chrStr.substring(3);	//in 69.head.CGmap, the format is 1, 2,etc. so we need to translate it.
				regions.add(new GenomeRegion(chr,bedFeature.getStart(),bedFeature.getEnd()));
			}
		}
		genomeRegionsProfiler(number_of_windows, regions);
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// read 70.head.CGmap
		CGmap cgmap = new CGmap("example/70.head.CGmap");
		CGmapProfiler cgmProfiler = new CGmapProfiler(cgmap);
		// calculate average methylation level with window length 10000 from 1 to 100000 (10 windows)
//		int windowlen=10000;
//		System.out.println("demo specific range");
//		cgmProfiler.windowProfiler(windowlen,"1",1,100000);
//		System.out.println("demo non-specific range");
//		cgmProfiler.windowProfiler(10000);
//		System.out.println("prepare for GeneProfiler, CpGProfiler, etc");
//		List<GenomeRegion> regions = new ArrayList<GenomeRegion>();
//		regions.add(new GenomeRegion("1",1,10000));
//		regions.add(new GenomeRegion("1",10001,20000));
//		regions.add(new GenomeRegion("1",20001,30000));
//		cgmProfiler.genomeRegionsProfiler(4, regions);
		cgmProfiler.bedFeaturesProfiler(4, "example/hg19_CpGIslands.bed");
	}
}