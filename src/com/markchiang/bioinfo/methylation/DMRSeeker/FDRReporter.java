/**
 * 
 */
package com.markchiang.bioinfo.methylation.DMRSeeker;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * The idea of this object is to record all the z-scores from both simulated and real data, 
 * sort the data by z-scores, and at the end, calculate the FDR for each z-scores. 
 * @author markchiang -Dosgi.requiredJavaVersion=1.7
 *
 */
public class FDRReporter {
	
	boolean bedformat = false;
	
	
	public boolean isBedformat() {
		return bedformat;
	}

	public void setBedformat(boolean bedformat) {
		this.bedformat = bedformat;
	}

	TreeSet<FDRRecord> records = new TreeSet<FDRRecord>(); 
	/**
	 * 
	 * @param zScore
	 * @param truePositive if from real data, should be noted truePositive, if from simulated data, should be noted false.
	 */
	public void add(double zScore , boolean truePositive){
		records.add(new FDRRecord(zScore,truePositive));
	}

	/**
	 * 
	 * @param fdrThreshold
	 * @return the maximum zScore which passed the FDR threshold
	 */
	public double getZScore(double fdrThreshold){
		long truePositive = 0;
		long falsePositive = 0;
		if(!bedformat)
		System.out.println("z-score\tFDR");
		for (FDRRecord record : records) {
			if (record.truePositive)
				truePositive++;
			else 
				falsePositive++;
			if (truePositive>0){
				double fdr = (double)falsePositive/(double)truePositive;
				if(!bedformat)
				System.out.println(record.zScore+"\t"+fdr);
			}else{
				if(!bedformat)
				System.out.println(record.zScore+"\t"+Double.POSITIVE_INFINITY);
			}
				
		}
		//report the smallest z-score which passed the FDR threshold. In this case, we need to reverse the order of records. and report the first one which passed the threshold.
		truePositive = 0;
		falsePositive = 0;
		double answer = Double.POSITIVE_INFINITY;
		double answerfdr = 0;
		for (FDRRecord record : records) {
			if (record.truePositive)
				truePositive++;
			else 
				falsePositive++;
			if (truePositive>0){
				double fdr = (double)falsePositive/(double)truePositive;
//				System.out.println(record.zScore+"\t"+fdr);
				if (fdr<=fdrThreshold){ 
					if (Math.abs(record.zScore) < Math.abs(answer)){
						answer = record.zScore;
						answerfdr = fdr;
					}
				}
			}else{
//				System.out.println(record.zScore+"\t"+Double.POSITIVE_INFINITY);
			}
				
		}
		if (answer < Double.POSITIVE_INFINITY){
			if(!bedformat)
			System.out.println("zScore set to "+ answer +" can give us FDR:" + answerfdr);
			return answer;
		}else
			return Double.NaN;
	}
}
