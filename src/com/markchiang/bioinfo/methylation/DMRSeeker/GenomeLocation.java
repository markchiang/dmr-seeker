/**
 * 
 */
package com.markchiang.bioinfo.methylation.DMRSeeker;

import com.markchiang.bioinfo.methylation.GenomeRegion;

/**
 * @author markchiang
 *
 */
public class GenomeLocation implements Comparable{
	String chr;
	Integer location;
	
	/**
	 * @param chr	Chromosome of this region
	 * @param location Position on the Chromosome
	 */
	public GenomeLocation(String chr, Integer location) {
		super();
		this.chr = chr;
		this.location = location;
	}

	@Override
	public int compareTo(Object other) throws ClassCastException{
		GenomeLocation otherLocation = (GenomeLocation)other;
		if (chr.equals(otherLocation.chr)){
			return location.compareTo(otherLocation.location);
		}else
			return chr.compareTo(otherLocation.chr);
	}
	
	@Override
	public String toString(){
		return chr+":"+location;
	}
	
	public boolean inThisRegion(GenomeRegion region){
		return region.getChr().equals(chr) && (region.getStart()<=location) && (region.getEnd()>=location);
	}
}