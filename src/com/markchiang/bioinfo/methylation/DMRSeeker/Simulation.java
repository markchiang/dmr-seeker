/**
 * 
 */
package com.markchiang.bioinfo.methylation.DMRSeeker;

import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import com.markchiang.bioinfo.methylation.BSSeeker2.CGmap;
import com.markchiang.bioinfo.methylation.BSSeeker2.CGmapMerger;
import com.markchiang.bioinfo.methylation.BSSeeker2.MethylationInfo;
import com.markchiang.bioinfo.methylation.DMRSeeker.DMRSeeker.Work;

/**
 * @author markchiang -Dosgi.requiredJavaVersion=1.7
 *
 */
public class Simulation {
	
	/**
	 * same as rbinom() in R, but written in Java
	 * @param count
	 * @param sampleSize
	 * @param prob
	 * @return
	 */
    public static int[] rbinom(int count, int sampleSize, double prob) {
        if(count <= 0 || prob < 0.0 || prob > 1.0)
        	return new int[0];
        
        int[] result = new int[count];
        int currentCount = 0;
        for(int i = 0; i < count; i++) {
            currentCount = 0;
            for(int j = 0; j < sampleSize; j++) {
                if(Math.random() < prob) {
                    currentCount++;
                }
            }
            result[i] = currentCount;
        }
        return result;
    }
   
    public Simulation(CGmapMerger group1, CGmapMerger group2) {
		TreeMap<String, TreeSet<Integer>> unionLocis = new TreeMap<String, TreeSet<Integer>>();  
		unionLocis.putAll(group1.getUnionLocis());
		unionLocis.putAll(group2.getUnionLocis());
		for (String chr : unionLocis.keySet()) {
			for (Integer loci : unionLocis.get(chr)){
				GenomeLocation genomeLocation = new GenomeLocation(chr,loci);
				//calculate methylation level for all libraries, unless there's missing data in a library.
				boolean dataMissing = false;
				List<CGmap> cgmaps1 = group1.getAllCGmaps();
				double sumMethylationLevel = 0;
				int sumLib = 0;
				for (CGmap cgmap : cgmaps1) {
					if (cgmap.getMethylationInfo(chr, loci) == null) { 
						dataMissing=true; 
						break;
					}
					sumMethylationLevel += cgmap.getMethylationInfo(chr, loci).getMethylationLevel(false);
					sumLib ++;
				}
				List<CGmap> cgmaps2 = group2.getAllCGmaps();
				for (CGmap cgmap : cgmaps2) {
					if (cgmap.getMethylationInfo(chr, loci) == null) { 
						dataMissing=true; 
						break;
					}
					sumMethylationLevel += cgmap.getMethylationInfo(chr, loci).getMethylationLevel(false);
					sumLib ++;
				}
				if (dataMissing) continue;	//if data missing, skip simulating this loci. 
				//TODO: site select: keep this site only if each group have more then two samples.
				//TODO: site select: coverage > threshold : for example coverage > 3
				double totalAverageMethylationLevel = sumMethylationLevel/sumLib;
				
				//start simulation
				for (CGmap cgmap : cgmaps1) {
					int coverage = cgmap.getMethylationInfo(chr, loci).getCoverage();
					int newC = rbinom(1,coverage,totalAverageMethylationLevel)[0];
					cgmap.getMethylationInfo(chr, loci).setSimulatedNumberOfC(newC);
//					double methylationLevel = cgmap.getMethylationInfo(chr, loci).getSimulatedLevel();
//					System.out.println("group1\t"+cgmap+"\t"+chr+"\t"+loci+"\t"+methylationLevel);
				}
				for (CGmap cgmap : cgmaps2) {
					int coverage = cgmap.getMethylationInfo(chr, loci).getCoverage();
					int newC = rbinom(1,coverage,totalAverageMethylationLevel)[0];
					cgmap.getMethylationInfo(chr, loci).setSimulatedNumberOfC(newC);
//					double methylationLevel = cgmap.getMethylationInfo(chr, loci).getSimulatedLevel();
//					System.out.println("group2\t"+cgmap+"\t"+chr+"\t"+loci+"\t"+methylationLevel);
				}
			}
		}
		
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] answer = rbinom(10,30,0.5);
		for (int i : answer) {
			System.out.println(i);
		}
	}
}
