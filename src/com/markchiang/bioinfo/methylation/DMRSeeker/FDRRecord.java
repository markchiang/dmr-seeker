/**
 * 
 */
package com.markchiang.bioinfo.methylation.DMRSeeker;

/**
 * @author markchiang -Dosgi.requiredJavaVersion=1.7
 *
 */
public class FDRRecord implements Comparable<FDRRecord>{
	double zScore;
	boolean truePositive;
	/**
	 * @param zScore
	 * @param truePositive
	 */
	public FDRRecord(double zScore, boolean truePositive) {
		super();
		this.zScore = zScore;
		this.truePositive = truePositive;
	}
	
	public int compareTo(FDRRecord o) {
		//sort descending, so we can calculate FDR easily in reporter
        return Double.compare(Math.abs(o.zScore),Math.abs(this.zScore));
    }
}
