/**
 * 
 */
package com.markchiang.bioinfo.methylation.DMRSeeker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.inference.TTest;

import com.markchiang.bioinfo.methylation.GenomeRegion;
import com.markchiang.bioinfo.methylation.BSSeeker2.CGmap;
import com.markchiang.bioinfo.methylation.BSSeeker2.CGmapMerger;

/**
 * DMR-Seeker looks for Differentially Methylated Regions for you.
 * @author markchiang
 * 
 */
public class DMRSeeker {

	CGmapMerger group1 = null;
	CGmapMerger group2 = null;
	
	Set<GenomeLocation> group1Peaks = null;
	Set<GenomeLocation> group2Peaks = null;
	
	TreeMap<GenomeLocation,Double> lociAverageMap1 = new TreeMap<GenomeLocation,Double>();
	TreeMap<GenomeLocation,Double> lociAverageMap2 = new TreeMap<GenomeLocation,Double>();
	
	TreeMap<GenomeLocation,Double> lociPValueMap = new TreeMap<GenomeLocation,Double>();
	TreeMap<GenomeLocation,Double> lociTScoreMap = new TreeMap<GenomeLocation,Double>();
	TreeMap<GenomeLocation,Double> lociDeltaMeanMap = new TreeMap<GenomeLocation,Double>();
	
	boolean debugmode = false;
	boolean bedformat = false;
	
	public boolean isBedformat() {
		return bedformat;
	}

	public void setBedformat(boolean bedformat) {
		this.bedformat = bedformat;
	}

	/**
	 * 
	 * @param group1 group1 in CGmapMerger format
	 * @param group2 group2 in CGmapMerger format
	 */
	public DMRSeeker(CGmapMerger group1, CGmapMerger group2) {
		super();
		this.group1 = group1;
		this.group2 = group2;
	}

	/**
	 * 
	 * @param thresholdPeak Genome locations with methylation level passed this threshold will be considered peaks 
	 * @param thresholdExtension The threshold for extending the circle, circle extension will stop at the nearest point which has methylation level lower then this value.
	 * @param thresholdPValue The threshold for maximum p-value
	 * @param thresholdZScore The threshold for minimum z-score
	 * @param thresholdWidth The threshold for minimum length of a Circle
	 */
	public Set<DMR> findDMR(double thresholdPeak, double thresholdExtension,
			double thresholdPValue, double thresholdZScore, int thresholdWidth, FDRReporter fdrReporter){
		group1Peaks = findPeaks(group1, group2,thresholdPeak, thresholdPValue, true);
		if (!bedformat)
		System.out.println("In simulated data:");
		if (!bedformat)
		System.out.println("found "+group1Peaks.size()+" peaks between group1 and group2");
		Set<GenomeRegion> circles = findCircles(group1, group2, group1Peaks, thresholdExtension);
		if (!bedformat)
		System.out.println("found "+circles.size()+" circles in group1 union group2");
		Set<DMR> circlesAfterFilter = filterCircles(circles,group1Peaks, group1, group2, thresholdPValue,thresholdZScore,thresholdWidth);
		if (!bedformat)
		System.out.println("found "+circlesAfterFilter.size()+" circles after filter");
		int limit = 100;
		if (!bedformat)
		System.out.println("start-end\tz-score");
		for (DMR circle : circlesAfterFilter) {
			fdrReporter.add(circle.getZScore(), false);
			if (debugmode) limit--;
			if (limit>0) 
				if (!bedformat)
				System.out.println(circle+"\t"+circle.getZScore());
		}
		return circlesAfterFilter;
	}

	/**
	 * 
	 * @param thresholdPeak Genome locations with methylation level passed this threshold will be considered peaks 
	 * @param thresholdExtension The threshold for extending the circle, circle extension will stop at the nearest point which has methylation level lower then this value.
	 * @param thresholdPValue The threshold for maximum p-value
	 * @param thresholdZScore The threshold for minimum z-score
	 * @param thresholdWidth The threshold for minimum length of a Circle
	 */
	public Set<DMR> findDMR2(double thresholdPeak, double thresholdExtension,
			double thresholdPValue, double thresholdZScore, int thresholdWidth, FDRReporter fdrReporter){
		group1Peaks = findPeaks(group1, group2,thresholdPeak, thresholdPValue, false);
		if (!bedformat)
		System.out.println("In real data:");
		if (!bedformat)
		System.out.println("found "+group1Peaks.size()+" peaks between group1 and group2");
		Set<GenomeRegion> circles = findCircles(group1, group2, group1Peaks, thresholdExtension);
		if (!bedformat)
		System.out.println("found "+circles.size()+" circles in group1 union group2");
		Set<DMR> circlesAfterFilter = filterCircles(circles,group1Peaks, group1, group2, thresholdPValue,thresholdZScore,thresholdWidth);
		if (!bedformat)
		System.out.println("found "+circlesAfterFilter.size()+" circles after filter");
		int limit = 100;
		if (!bedformat)
		System.out.println("start-end\tz-score");
		for (DMR circle : circlesAfterFilter) {
			fdrReporter.add(circle.getZScore(), true);
			if (debugmode) limit--;
			if (limit>0) 
				if (!bedformat)
				System.out.println(circle+"\t"+circle.getZScore());
		}
		return circlesAfterFilter;
	}

	/**
	 * Filter the circles using thresholds.
	 * @param circles
	 * @param peaks
	 * @param thresholdPValue
	 * @param thresholdZScore
	 * @param thresholdLength
	 * @return
	 */
	//TODO: return not only GenomeRegion but also z-score ...(create a DMR object which contains GenomeRegion, z-score, delta, ...)
	private Set<DMR> filterCircles(Set<GenomeRegion> circles, Set<GenomeLocation> peaks, CGmapMerger group1, CGmapMerger group2, double thresholdPValue, double thresholdZScore,
			int thresholdLength) {
		//TODO: currently only thresholdLength and thresholdZScore implemented.
		Set<DMR> result = new TreeSet<DMR>();
		
		// mean_t: average of all the t-score we got.
		double total = 0;
		int count = 0;
		double values[] = new double[lociTScoreMap.size()];
		int index = 0;
		for (Double value : lociTScoreMap.values()){
			if ((!value.isNaN())&&(!value.isInfinite())){	//TODO: When two groups has standard deviation == 0, t-score will be NaN.)
				values[index++]=value;
				total+= value;
				count++;
			}
		}
		double mean_t = total/count;

		// std_t: standard deviation of all the t-score we got.
		
		double std_t = new StandardDeviation().evaluate(values,0,count);

		for (GenomeRegion circle : circles) {
			boolean keep = true;
			//TODO: DMR width filter: instead of using base as unit, use how many sites in this range as unit.
			if (circle.length() < thresholdLength){
				keep = false;
				continue;	// since it's already been determined should be filtered, to speed up, we can skip this circle for further calculation.
			}
			
			/* implement z=math.sqrt(nt)*float(average_t-mean_t)/std_t in R */

			// find peaks within this circle
//			TreeSet<GenomeLocation> peaksInCircle = new TreeSet<GenomeLocation>();
//			for (GenomeLocation peak : peaks) {
//				if (circle.contains(peak)){
//					peaksInCircle.add(peak);
//				};				
//			}
			// nt: number of t-scores we have within this range.
			TreeSet<GenomeLocation> tScoresInCircle = new TreeSet<GenomeLocation>();
			double sum = 0;
			for (GenomeLocation loci : lociTScoreMap.keySet()) {
				if (circle.contains(loci)){
					if (!lociTScoreMap.get(loci).isNaN()){
						tScoresInCircle.add(loci);
						sum+= lociTScoreMap.get(loci);
					}
				};
			}
			int nt = tScoresInCircle.size();
			// average_t: average of the t-score within this range.
			double average_t = sum/tScoresInCircle.size();
			
			double z = Math.sqrt(nt)*(average_t-mean_t)/std_t;
			
			if (Math.abs(z) < thresholdZScore){
				keep = false;
				continue;
			}
			
			if (keep==true)
				result.add(new DMR(circle,z));
			//TODO: calculate delta_m for DMR region instead of only use z-score.
		}
		return result;
	}

	/**
	 * For each peak, look it's neighbor base's delta_mean on records, if pass threshold, extend the circle.
	 * @param group1
	 * @param group2
	 * @param peaks
	 * @param thresholdExtension
	 * @return
	 */
	//TODO: now only consider hyper, we should also consider hypo.
	private Set<GenomeRegion> findCircles(CGmapMerger group1, CGmapMerger group2, Set<GenomeLocation> peaks, double thresholdExtension) {
		Set<GenomeRegion> result = new TreeSet<GenomeRegion>();
		// for each peak, try to extend the circle until the value drops below the threshold.
		for (GenomeLocation location : peaks){

			Entry<GenomeLocation,Double> upperbound = null;
			Entry<GenomeLocation,Double> lowerbound = null;
			double peakvalue = lociDeltaMeanMap.get(location);
			Entry<GenomeLocation,Double> higherentry = lociDeltaMeanMap.ceilingEntry(location);
			upperbound = higherentry;
			while(sameDirectionAndPassThreshold(higherentry.getValue(),peakvalue,thresholdExtension)){
				upperbound = higherentry;	//pass the threshold, save the upper bound
				higherentry = lociDeltaMeanMap.higherEntry(higherentry.getKey());	//prepared for next round
			}
			
			Entry<GenomeLocation,Double> lowerentry = lociDeltaMeanMap.floorEntry(location);
			lowerbound = lowerentry;
			while(sameDirectionAndPassThreshold(lowerentry.getValue(),peakvalue,thresholdExtension)){
				lowerbound = lowerentry;	//pass the threshold, save the lower bound
				lowerentry = lociDeltaMeanMap.lowerEntry(lowerentry.getKey());	//prepared for next round
			}
			
			GenomeRegion circle = new GenomeRegion(location.chr,lowerbound.getKey().location,upperbound.getKey().location);
			result.add(circle);
		}
		
		return result;
	}

	private boolean sameDirectionAndPassThreshold(double value, double peak, double threshold){
		if (value>0 && peak>0){
			return (value >= threshold);
		}else if (value<0 && peak<0){
			return (Math.abs(value) >= threshold);
		}
		return false;
	}
	
	private boolean isPeak(CGmapMerger group1, CGmapMerger group2, double thresholdPeak, double thresholdPValue, GenomeLocation genomeLocation, boolean simulation) {
		//collect data from group1
		String chr = genomeLocation.chr;
		Integer loci = genomeLocation.location;
		List<Double> group1values = new ArrayList<Double>();
		//test if anything missing
		for (CGmap cgmap : group1.getAllCGmaps()){
			if (cgmap.getChrMap().get(chr).get(loci) == null) continue;
			Number value = cgmap.getChrMap().get(chr).get(loci).getMethylationLevel(simulation);
			if (value!=null){
				group1values.add(cgmap.getChrMap().get(chr).get(loci).getMethylationLevel(simulation));
			}
		}
		//collect data from group2
		List<Double> group2values = new ArrayList<Double>(); 
		for (CGmap cgmap : group2.getAllCGmaps()){
			if (cgmap.getChrMap().get(chr).get(loci) == null) continue;
			Number value = cgmap.getChrMap().get(chr).get(loci).getMethylationLevel(simulation);
			if (value!=null){
				group2values.add(cgmap.getChrMap().get(chr).get(loci).getMethylationLevel(simulation));
			}
		}
		if ((group1values.size()>=2)&&(group2values.size()>=2)){ // need at least sample size >= 2 to calculate p-value

			//convert array of Double to array of double
			double[] values1 = new double[group1values.size()];
			double sum1 = 0;
			for (int i = 0; i < values1.length; i++) {	
				values1[i] = group1values.get(i);
				sum1+= values1[i];
			}
			//convert array of Double to array of double
			double[] values2 = new double[group2values.size()];
			double sum2 = 0;
			for (int i = 0; i < values2.length; i++) {
				values2[i] = group2values.get(i);
				sum2+= values2[i];
			}
			double p_value = new TTest().tTest(values1,values2);	//calculate p-value, will return NaN if two groups are the same.
			double t_score = new TTest().t(values1,values2);	//calculate t-value, will return NaN if two groups are the same.
			synchronized (lociPValueMap) {
				lociPValueMap.put(genomeLocation, p_value);
				lociTScoreMap.put(genomeLocation, t_score);
			}
			double delta_mean = (sum1/group1values.size())-(sum2/group2values.size());	//calculate delta_mean
			synchronized (lociDeltaMeanMap) {
				lociDeltaMeanMap.put(genomeLocation, delta_mean);
			}
			if ((p_value <= thresholdPValue)&&(Math.abs(delta_mean)>=thresholdPeak))
				return true;
			else 
				return false;
		}else return false;	
	}
	
	public class Work implements Runnable {  
		CGmapMerger group1;
		CGmapMerger group2;
		double thresholdPeak;
		double thresholdPValue;  
		GenomeLocation genomeLocation;
		Set<GenomeLocation> answer;
		boolean simulation;
		       
		public Work(CGmapMerger group1, CGmapMerger group2, double thresholdPeak, double thresholdPValue,
				GenomeLocation genomeLocation, Set<GenomeLocation> answer, boolean simulation) {
			super();
			this.group1 = group1;
			this.group2 = group2;
			this.thresholdPeak = thresholdPeak;
			this.thresholdPValue = thresholdPValue;
			this.genomeLocation = genomeLocation;
			this.answer = answer;
			this.simulation = simulation;
		}
		@Override
		public void run() {
			if (isPeak(group1, group2, thresholdPeak, thresholdPValue, genomeLocation, simulation))
				synchronized(answer){
					answer.add(genomeLocation);
				}
		}
	}
	
	private Set<GenomeLocation> findPeaks(CGmapMerger group1, CGmapMerger group2, double thresholdPeak, double thresholdPValue, boolean simulation) {
		Set<GenomeLocation> answer = new TreeSet<GenomeLocation>();
		
		//get union locis from either group
		TreeMap<String, TreeSet<Integer>> unionLocis = new TreeMap<String, TreeSet<Integer>>();  
		unionLocis.putAll(group1.getUnionLocis());
		unionLocis.putAll(group2.getUnionLocis());
		
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		
		for (String chr : unionLocis.keySet()) {
			for (Integer loci : unionLocis.get(chr)){
				GenomeLocation genomeLocation = new GenomeLocation(chr,loci);
				executor.execute(new Work(group1,group2,thresholdPeak,thresholdPValue,genomeLocation,answer,simulation));
			}
		}
		
		//waiting for threads to finish.
		executor.shutdown();
        while (!executor.isTerminated()) {
        	try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
		return answer;
	}

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws IOException, ParseException {
		if (args.length<6) {
			System.err.println("DMR-Seeker Version 0.20");
			System.err.println("Usage: java -jar DMRSeeker.jar [-bed] -peak 0.2 -ext 0.0 -zscore 10 -width 10 -FDR 0.1 -E mt1.CGmap.CG mt2.CGmap.CG mt3.CGmap.CG ... -C wt1.CGmap.CG wt2.CGmap.CG wt3.CGmap.CG ...");
			System.exit(1);
		}
		Options options = new Options();
		options.addOption("E", true, "files belongs to experiment group");
		options.getOption("E").setArgs(Option.UNLIMITED_VALUES);
		options.addOption("C", true, "files belongs to control group");
		options.getOption("C").setArgs(Option.UNLIMITED_VALUES);
		options.addOption("peak", true, "threshold for finding peaks");
		options.addOption("ext", true, "threshold for extention");
		options.addOption("zscore", true, "threshold for filter circles");
		options.addOption("width", true, "threshold for filter circles");
		options.addOption("FDR", true, "threshold for FDR");
		options.addOption("bed", false, "output final result in bed format without any other information");
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse( options, args);
		String peakStr = cmd.getOptionValue("peak");
		Double peak = Double.parseDouble(peakStr);
		String extStr = cmd.getOptionValue("ext");
		Double ext = Double.parseDouble(extStr);
		String zscoreStr = cmd.getOptionValue("zscore");
		Double zscore = Double.parseDouble(zscoreStr);
		String widthStr = cmd.getOptionValue("width");
		String fdrStr = cmd.getOptionValue("FDR");
		Double fdr = Double.parseDouble(fdrStr);
		Integer width = Integer.parseInt(widthStr);
		String[] experiments = cmd.getOptionValues("E");
		String[] controls = cmd.getOptionValues("C");
		Boolean bedformat = cmd.hasOption("bed");
		
		CGmapMerger experimentGroup = new CGmapMerger();
		for (String filename : experiments) {
			CGmap cgmap = new CGmap(filename);
			experimentGroup.merge(cgmap);
		}

		CGmapMerger controlGroup = new CGmapMerger();
		for (String filename : controls) {
			CGmap cgmap = new CGmap(filename);
			controlGroup.merge(cgmap);
		}
				
		DMRSeeker dmrseeker = new DMRSeeker(experimentGroup, controlGroup);
		dmrseeker.setBedformat(bedformat);
		Simulation sim = new Simulation(experimentGroup, controlGroup);
		FDRReporter fdrReporter = new FDRReporter();
		fdrReporter.setBedformat(bedformat);
		//TODO: p-value threshold 0.05 should be user defined.
		dmrseeker.findDMR(peak,ext,0.05,zscore,width,fdrReporter);	//simulation
		System.gc();
		//TODO: remember the DMRs and report only the DMR passed the zScore.
		Set<DMR> dmrs = dmrseeker.findDMR2(peak,ext,0.05,zscore,width,fdrReporter);	//real data
		double zScore = fdrReporter.getZScore(fdr);
		if (Double.isNaN(zScore)){
			if (bedformat)
			System.err.println("Can not find a zScore which satisfy the FDR threshold.");
			else
			System.out.println("Can not find a zScore which satisfy the FDR threshold.");
		}else{
			if (!bedformat){
			System.out.println("Final Answer:");
			System.out.println("Region\tLength\tz-score");
			}
			for (DMR dmr : dmrs) {
				if (Math.abs(dmr.z_score) >= Math.abs(zScore)){
					if (bedformat){
						System.out.print(dmr.getGenomeRegion().toBed());
					}else{
						System.out.print(dmr.getGenomeRegion());
					}
					System.out.print("\t");
					System.out.print(dmr.getGenomeRegion().length());
					System.out.print("\t");
					System.out.print(dmr.getZScore());
					System.out.println();
				}
			}
		}		
	}
}