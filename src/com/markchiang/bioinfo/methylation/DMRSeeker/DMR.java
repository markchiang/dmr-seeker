/**
 * 
 */
package com.markchiang.bioinfo.methylation.DMRSeeker;

import java.util.Set;

import com.markchiang.bioinfo.methylation.GenomeRegion;

/**
 * @author markchiang -Dosgi.requiredJavaVersion=1.7
 *
 */
public class DMR implements Comparable{
	GenomeRegion genomeRegion;
	double z_score;
	//TODO: store delta_m of DMR and report it in the final report
	/**
	 * @return the genomeRegions
	 */
	public GenomeRegion getGenomeRegion() {
		return genomeRegion;
	}
	/**
	 * @return the z_score
	 */
	public double getZScore() {
		return z_score;
	}
	/**
	 * @param genomeRegions
	 * @param z_score
	 */
	public DMR(GenomeRegion genomeRegion, double z_score) {
		super();
		this.genomeRegion = genomeRegion;
		this.z_score = z_score;
	}
	@Override
	public int compareTo(Object other) {
		GenomeRegion otherRegion = ((DMR)other).getGenomeRegion();
		if (genomeRegion.getChr().equals(otherRegion.getChr())){
			if (genomeRegion.getStart().compareTo(otherRegion.getStart())==0)
				return genomeRegion.getEnd().compareTo(otherRegion.getEnd());
			else 
				return genomeRegion.getStart().compareTo(otherRegion.getStart());
		}else
			return genomeRegion.getChr().compareTo(otherRegion.getChr());
	}
	@Override
	public String toString(){
		return (genomeRegion.getChr()+":"+genomeRegion.getStart()+"-"+genomeRegion.getEnd());
	}

}
